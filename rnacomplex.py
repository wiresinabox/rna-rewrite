#This python modules holds the classes and data structures to hold the data parsed from the hb2, nb2 and json files.
#All data will revolve around the residue level. Each residue will then hold the bonds associated with them. 

#Ground Rules:
#If position doesn't matter or is already defined by a keyword like (K0012) then use DICTS not LISTS
#You can use list for things like one-layer-deep data dumps for checking using 'in'.
#Keep it simple. Don't be insane and make a 7 layer deep list you psycho. This is what this is for.
#__repr__ everything so debug is a lot simpler.

#ORGANIZATION:
#complex:
#   residue database: residues...
#       residue:
#           donordict {'B0002':bond}
#               bond:
#                  attribues and functions to parse hbplus data.
#           receptordict {'A1234':bond}

#Version 0.01

import json


printResidueBonds = False #Turns on/off printing all bonds in a residue when printed.

#General translation functions
def DSSRtoPDBid(dssrId):
    #Translates DSSR ids (E.G27) to PDB/HBPLUS standard (E27, resType: G)
    #Returns a tuple of (ID, resType)
    #The Residue list is for nonstandard residues, which screws with the id length.
    chain = dssrId[0]
    
    resType = dssrId[2]

    index = dssrId[3:]
    #For nonstandard residues
    if '/' in dssrId:
        resType, index = dssrId[2:].split('/')
    else:
        try:
            int(dssrId[3])
        except:
            resType = dssrId[2:4]
            index = dssrId[4:]
        try:
            int(dssrId[4])
        except:
            resType = dssrId[2:5]
            index = dssrId[5:]

    return (chain+index, resType)
def canonTag(canonSymbol):
    if canonSymbol == '|':
        return "cannonical"
    if canonSymbol == '.':
        return "noncanonical"
    else:
        print('Unknown canon symbol: {}'.format(canonSymbol))
def helixTypeTag(helixSymbol, prefix):
    if helixSymbol == 'A':
        return prefix+'type-A'
    elif helixSymbol == 'B':
        return prefix+'type-B'
    elif helixSymbol == 'Z':
        return prefix+'type-Z'
    elif helixSymbol == 'x':
        return prefix+'type-broken'
    elif helixSymbol == '.':
        return prefix+'type-unknown'
    else:
        print('unknown helixSymbol: {}'.format(helixSymbol))


class bond:
    bondType = None #HB or VdW
    category = None #For later assignment 

    donorChain = None #K
    donorPos = None #27
    donorId = None #K27
    donorRes = None #G
    donorAtom = None #O
    donorAtomSpecifier = None #6'
    donorAtomFull = None #O6'

    acceptorChain = None #K
    acceptorPos = None #27
    acceptorId = None #K27
    acceptorRes = None #G
    acceptorAtom = None #O
    acceptorAtomSpecifier = None #6'
    acceptorAtomFull = None #O6'

    daDist = None #Donor to Acceptor Distance

    donorChainCat = None #M - Main Chain, S - Side Chain, H - Heteroatom
    acceptorChainCat = None
    
    daAADist = None #Amino acid distance if same chain. -2 if not.
    daCAdistance = None #Distance between central Alpha Carbons
    daHangle = None #Donor Acceptor angle from hydrogen -1 if no hydrogen
    ahDist = None #Hydrogen Acceptor Distance. -1 if No Hydrogen
    ahAAangle = None #Donor Acceptor angle from the Acceptor Atom.
    daAAangle = None #Donor Acceptor Anteceedent Angle at Acceptor.

    bondNumber = None

    rawString = None


    def __init__ (self, rawString, bondType):
        self.category = -1
        self.setByString(rawString)
        if bondType == 'HB' or bondType == 'VdW':
            self.bondType = bondType
        else:
            print('Bondtype not HB or VdW!')
            
        

    def __repr__(self):
        return '({}|{}|{}|{})-({}|{}|{}|{})'.format(str(self.donorChainCat), str(self.donorId), str(self.donorRes), str(self.donorAtomFull), str(self.acceptorAtomFull), str(self.acceptorRes), str(self.acceptorId), str(self.acceptorChainCat))

    def setByString(self, rawString):
    #Parses string and sets values gleaned from it.

       self.donorChain = rawString[0] #K
       self.donorPos = rawString[1:5].lstrip('0 ') #2
       self.donorId = self.donorChain + self.donorPos #K27
       #print(self.donorChain, self.donorPos, self.donorId, rawString[0:10])
       self.donorRes = rawString[6:9].strip() #G
       self.donorAtom = rawString[10] #O
       self.donorAtomSpecifier = rawString[11:13].strip() #6'
       self.donorAtomFull = self.donorAtom + self.donorAtomSpecifier #O6'

       self.acceptorChain = rawString[14] #K
       self.acceptorPos = rawString[15:19].lstrip('0 ') #27
       self.acceptorId = self.acceptorChain + self.acceptorPos #K27
       #print(self.acceptorChain, self.acceptorPos, self.acceptorId, rawString[14:19])
       self.acceptorRes = rawString[20:23].strip() #G
       self.acceptorAtom = rawString[24] #O
       self.acceptorAtomSpecifier = rawString[25:27].strip() #6'
       self.acceptorAtomFull = self.acceptorAtom + self.acceptorAtomSpecifier #O6'

       self.daDist = rawString[28:32] #Donor to Acceptor Distance

       self.donorChainCat = rawString[33] #M - Main Chain, S - Side Chain, H - Heteroatom
       self.acceptorChainCat = rawString[34]
       self.daAADist = rawString[36:39] #Amino acid distance if same chain. -2 if not.
       self.daCAdistance = rawString[40:45] #Distance between central Alpha Carbons
       self.daHangle = rawString[46:51] #Donor Acceptor angle from hydrogen -1 if no hydrogen
       self.ahDist = rawString[52:57] #Hydrogen Acceptor Distance. -1 if No Hydrogen
       self.ahAAangle = rawString[58:63] #Donor Acceptor angle from the Acceptor Atom.
       self.daAAangle = rawString[64:69] #Donor Acceptor Anteceedent Angle at Acceptor.

       self.bondNumber = rawString[70:75]

       self.rawString = rawString

class residue:
    chain=None #K
    chainPos=None #10
    resId=None #K10
    resType = None #G
    atomList = None #[N, CA, C, O, CB...]
    donorHBDict = None #Key -> resId of the recipient.
    donorVdWDict = None 
    acceptorHBDict = None
    acceptorVdWDict = None

    containsBond = False
    containsDonorHB = False
    containsDonorVdW = False
    containsAcceptorHB = False
    containsAcceptorVdW = False

    donorHBCount = None #Updated at the time of bond addition for easy reference.
    donorVdWCount = None
    acceptorHBCount = None
    acceptorVdWCount = None

    donorHBCountValid = None 
    donorVdWCountValid = None
    acceptorHBCountValid = None
    acceptorVdWCountValid = None

    structTagList = None # A list of DSSR tags assigned to the residue.


    def __init__ (self, resId):
        #Instance variables have to initialized in here, othewise, they become
        #class specific and ALL instances can access them.
        self.atomList = [] #[N, CA, C, O, CB...]
        self.donorHBDict = {} #Key -> resId of the recipient. Returns a list of bonds attached to that res.
        self.donorVdWDict = {} 
        self.acceptorHBDict = {}
        self.acceptorVdWDict = {}
        self.donorHBCount = 0 #Updated at the time of bond addition for easy reference.
        self.donorVdWCount = 0
        self.acceptorHBCount = 0
        self.acceptorVdWCount = 0

        self.donorHBCountValid = 0 
        self.donorVdWCountValid = 0
        self.acceptorHBCountValid = 0
        self.acceptorVdWCountValid = 0
        self.resId = resId
        self.chain = resId[0]
        self.chainPos = int(resId[1:])

        self.structTagList = [] # A list of DSSR tags assigned to the residue.

    def __repr__ (self):
        returnString = "Id: {} | Type: {} |\n".format(self.resId, self.resType)
        returnString = returnString + "Atoms: "
        for atom in self.atomList:
            returnString = returnString + str(atom) + ', '
        returnString = returnString + "\n"

        
        returnString = returnString + "All Bonds: dHB: {}, dVdW: {}, aHB: {}, aVdW: {} \n".format(self.donorHBCount, self.donorVdWCount, self.acceptorHBCount, self.acceptorVdWCount)
        returnString = returnString + "Valid Bonds: dHB: {}, dVdW: {}, aHB: {}, aVdW: {} \n".format(self.donorHBCountValid, self.donorVdWCountValid, self.acceptorHBCountValid, self.acceptorVdWCountValid)
        
        returnString = returnString + 'Tags: '
        for tag in self.structTagList:
            returnString = returnString + str(tag) + ', '
        returnString = returnString +'\n'

        if printResidueBonds == True:
            returnString = returnString + "Donor HB Bonds: \n"
            for res in self.donorHBDict.values():
                for bond in res:
                    returnString = returnString + str(bond)
                    returnString = returnString + "\n"
            returnString = returnString + "Donor VdW Bonds: \n"
            for res in self.donorVdWDict.values():
                for bond in res:
                    returnString = returnString + str(bond)
                    returnString = returnString + "\n"
            returnString = returnString + "Acceptor HB Bonds: \n"
            for res in self.acceptorHBDict.values():
                for bond in res:
                    returnString = returnString + str(bond)
                    returnString = returnString + "\n"
            returnString = returnString + "Acceptor VdW Bonds: \n"
            for res in self.acceptorVdWDict.values():
                for bond in res:
                    returnString = returnString + str(bond)
                    returnString = returnString + "\n"
        return returnString

    def addBond(self, bond):
        #Adds the bond to the appropriate dictionary.
        donorId = bond.donorId
        acceptorId = bond.acceptorId
        bondType = bond.bondType

        if donorId == self.resId:
            if bondType == 'HB':
                if acceptorId in self.donorHBDict:
                    self.donorHBDict[acceptorId].append(bond)
                else:
                    self.donorHBDict[acceptorId] = [bond]
                self.donorHBCount += 1
                self.containsDonorHB = True
                self.containsBond = True
            elif bondType == 'VdW':
                if acceptorId in self.donorVdWDict:
                    self.donorVdWDict[acceptorId].append(bond)
                else:
                    self.donorVdWDict[acceptorId] = [bond]
                self.donorVdWCount += 1
                self.containsDonorVdW = True
                self.containsBond = True
            else:
                print("Bond Type Unknnown, Gives: " + str(bondType))

        elif acceptorId == self.resId:
            if bondType == 'HB':
                if donorId in self.acceptorHBDict:
                    self.acceptorHBDict[donorId].append(bond)
                else:
                    self.acceptorHBDict[donorId] = [bond]
                self.acceptorHBCount += 1
                self.containsAcceptorHB = True
                self.containsBond = True
            elif bondType == 'VdW':
                if donorId in self.acceptorVdWDict:
                    self.acceptorVdWDict[donorId].append(bond)
                else:
                    self.acceptorVdWDict[donorId] = [bond]
                self.acceptorVdWCount += 1
                self.containsAcceptorVdW = True
                self.containsBond = True
            else:
                print("Bond Type Unknnown, Gives: " + str(bondType))
        else:
            print('Bond not assigned to this residue. ResId: {}, DonorId: {}, AcceptorId {}'.format(self.resId, donorId, acceptorId))
        
    def isBondIn(self, verbose=False):
        #Checks if Bond is in one of the dictonaries.
        #If verbose is false, then returns true or false if the bond is in one of tthe dictionaries. 
        #If verbose is true, then returns which dictionary it is in.
        if verbose == False:
            bondExists = False
            if bond in donorHBDict:
                bondExists = True
            if bond in donorVdWDict:
                bondExists = True
            if bond in acceptorHBDict:
                bondExists = True
            if bond in acceptorVdWDict:
                bondExists = True
            return bondExists
                
        elif verbose == True:
            if bond in donorHBDict:
                return 'donorHBDict'
            if bond in donorVdWDict:
                return 'donorVdWDict'
            if bond in acceptorHBDict:
                return 'acceptorHBDict'
            if bond in acceptorVdWDict:
                return 'acceptorVdWDict'
            return False


class rnacomplex:
    complexName = None
    

    
    #Lists of possible chain ids, residue types etc...
    chainIdCountDict = None #K:204
    resTypeCountDict = None #U34:12, G:206
    
    bondCount = None
    donorHBBondCount = None
    donorVdWBondCount = None
    acceptorHBBondCount = None
    acceptorVdWBondCount = None

    dssrStructDict = None #helix:{set of resIds}

    residueDict = None #resId:residue(class). This is the main dictionary.

    def __init__ (self, name, pdbFile, dssrJson, hb2, nb2):
        self.chainIdCountDict = {} #K:204
        self.resTypeCountDict = {} #U34:12, G:206
        self.bondCount = 0
        self.donorHBBondCount = 0
        self.donorVdWBondCount = 0
        self.acceptorHBBondCount = 0
        self.acceptorVdWBondCount = 0
        self.dssrStructDict = {} #helix:{set of resIds}
        self.residueDict = {} #resId:residue(class). This is the main dictionary.

        self.complexName = name
        self.populateResidues(pdbFile)
        #print('Residues populated!')
        self.populateBondsfromHBplus(hb2, 'HB')
        self.populateBondsfromHBplus(nb2, 'VdW')
        self.populateStructfromDSSR(dssrJson)
        

    def populateResidues(self, pdbFile):
        #Runs through the PDB and populates residueDict with all of the residues.
        pdbLines = [ln for ln in pdbFile.readlines() if ln.startswith('ATOM') or ln.startswith('HETATM')]
        for line in pdbLines:
            atomFull = line[13:16].strip() #O5'
            resType = line[17:20] #U34, G
            chain = line[21]
            resPos = line[22:26].lstrip('0 ')
            
            resId = chain + resPos
            if resId not in self.residueDict:
                #Initialize a new residue class and add it to the dictionary.
                res = residue(resId)
                self.residueDict[resId] = res
                #print('Adding {} to residueDict'.format(resId))
            else:
                res = self.residueDict[resId]
            #Add the atom to the residue's atom list
            res.atomList.append(atomFull)
            res.resType = resType

            if chain in self.chainIdCountDict:
                self.chainIdCountDict[chain] +=1
            else:
                self.chainIdCountDict[chain] = 1
            
            if resType in self.resTypeCountDict:
                self.resTypeCountDict[resType] +=1
            else:
                self.resTypeCountDict[resType] = 1
    
    def addBondtoResidues(self, bond):
        #Adds the bond to the appropriate residue
        donorId = bond.donorId
        acceptorId = bond.acceptorId
        bondType = bond.bondType
        try: 
            donorRes = self.residueDict[donorId]
        except KeyError:
            print("{} doesn't exist in the residueDict as a donor, did you populate it? Skipping.".format(donorId))
            return
        try: 
            acceptorRes = self.residueDict[acceptorId]
        except KeyError:
            print("{} doesn't exist in the residueDict as an acceptor, did you populate it? Skipping".format(acceptorId))
            return
        
        donorRes.addBond(bond)
        acceptorRes.addBond(bond)
    
    def populateBondsfromHBplus(self, hbplusfile, hbplustype):
        #hbplusfile is either the nb2 or hb2, hbplustype is either 'HB' or 'VdW'
        lines = hbplusfile.readlines()
        #print(len(lines), hbplustype)
        del lines[0:8]
        for line in lines:
            #initialize bond
            newBond = bond(line, hbplustype)
            #print('Adding a {} | {}'.format(hbplustype, newBond.donorId))
            self.addBondtoResidues(newBond)

    def addStructTag(self, tag, residueId):
        res = self.residueDict[residueId]
        res.structTagList.append(tag)
        if tag in self.dssrStructDict:
            tagSet = self.dssrStructDict[tag]
            tagSet.add(residueId)
        else:
            self.dssrStructDict[tag] = set()
            self.dssrStructDict[tag].add(residueId)


    def populateStructfromDSSR(self, jsonfile):
        jsonstr = jsonfile.readline()
        jsondict = json.loads(jsonstr)
        #Each complex has a different set of feature tags so check if they exist
        if 'helices' in jsondict:
            heliciesDict = jsondict['helices']
            #Helicies, Stems, Iloops bit:
            for helix in heliciesDict:
                helixIndex = helix['index']
                strand1 = helix['strand1']
                strand2 = helix['strand2']
                bpTypeStr = helix['bp_type'] #'|' is cannonical, '.' is otherwise
                helixTypeStr = helix['helix_form'] #A, B, Z Helix type, '.' for unknown, x is broken backbone
                pairs = helix['pairs'] #This is the important dict.
                for pair in pairs:
                    pairIndex = pair['index']
                    strIndex = pairIndex - 1
                    resId1, resType1 = DSSRtoPDBid(pair['nt1'])
                    resId2, resType2 = DSSRtoPDBid(pair['nt2'])
                    pairCanon = bpTypeStr[strIndex]
                    if strIndex == len(helixTypeStr): #Final index does not have a helix form for some reason.
                        pairHelixType = 'None'
                    else:
                        pairHelixType = helixTypeStr[strIndex]
                     
                    #Populate the residues with tags.
                    self.addStructTag('helix', resId1) 
                    self.addStructTag('helix', resId2) 
                    self.addStructTag(canonTag(pairCanon), resId1) 
                    self.addStructTag(canonTag(pairCanon), resId2) 
                    self.addStructTag('helix-'+canonTag(pairCanon), resId1) 
                    self.addStructTag('helix-'+canonTag(pairCanon), resId2)
                    if pairHelixType != 'None':
                        self.addStructTag(helixTypeTag(pairHelixType, 'helix'), resId1) 
                        self.addStructTag(helixTypeTag(pairHelixType, 'helix'), resId2) 

                
        if 'stems' in jsondict:
            stemsDict = jsondict['stems']
            for helix in stemsDict:
                helixIndex = helix['index']
                strand1 = helix['strand1']
                strand2 = helix['strand2']
                bpTypeStr = helix['bp_type'] #'|' is cannonical, '.' is otherwise
                helixTypeStr = helix['helix_form'] #A, B, Z Helix type, '.' for unknown, x is broken backbone
                pairs = helix['pairs'] #This is the important dict.
                for pair in pairs:
                    pairIndex = pair['index']
                    strIndex = pairIndex - 1
                    resId1, resType1 = DSSRtoPDBid(pair['nt1'])
                    resId2, resType2 = DSSRtoPDBid(pair['nt2'])
                    pairCanon = bpTypeStr[strIndex]
                    if strIndex == len(helixTypeStr): #Final index does not have a helix form for some reason.
                        pairHelixType = 'None'
                    else:
                        pairHelixType = helixTypeStr[strIndex]
                     
                    #Populate the residues with tags.
                    self.addStructTag('stem', resId1) 
                    self.addStructTag('stem', resId2) 
                    self.addStructTag(canonTag(pairCanon), resId1) 
                    self.addStructTag(canonTag(pairCanon), resId2) 
                    self.addStructTag('stem-'+canonTag(pairCanon), resId1) 
                    self.addStructTag('stem-'+canonTag(pairCanon), resId2)
                    if pairHelixType != 'None':
                        self.addStructTag(helixTypeTag(pairHelixType, 'stem'), resId1) 
                        self.addStructTag(helixTypeTag(pairHelixType, 'stem'), resId2) 
                
        if 'iloops' in jsondict:
            loops = jsondict['iloops']
            for loopDict in loops:
                print(loopDict)
                loopIndex = loopDict['index']
                nucleotideStr = loopDict['nts_long'] #Contains the nucleotides apart of the loop:
                nucleoList = nucleotideStr.split(',')
                for nts in nucleoList:
                    resId, resType = DSSRtoPDBid(nts)
                    self.addStructTag('iloop', resId)

        if 'hairpins' in jsondict:
            loops = jsondict['hairpins']
            for loopDict in loops:
                loopIndex = loopDict['index']
                nucleotideStr = loopDict['nts_long'] #Contains the nucleotides apart of the loop:
                nucleoList = nucleotideStr.split(',')
                for nts in nucleoList:
                    resId, resType = DSSRtoPDBid(nts)
                    self.addStructTag('hairpin', resId)

        if 'bulges' in jsondict:
            loops = jsondict['bulges']
            for loopDict in loops:
                loopIndex = loopDict['index']
                nucleotideStr = loopDict['nts_long'] #Contains the nucleotides apart of the loop:
                nucleoList = nucleotideStr.split(',')
                for nts in nucleoList:
                    resId, resType = DSSRtoPDBid(nts)
                    self.addStructTag('bulges', resId)

        if 'junctions' in jsondict:
            loops = jsondict['junctions']
            for loopDict in loops:
                loopIndex = loopDict['index']
                nucleotideStr = loopDict['nts_long'] #Contains the nucleotides apart of the loop:
                nucleoList = nucleotideStr.split(',')
                for nts in nucleoList:
                    resId, resType = DSSRtoPDBid(nts)
                    print(nts, resId)
                    self.addStructTag('junctions', resId)
    
