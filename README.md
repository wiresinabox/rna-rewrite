OH OK. Version 0.01

So rnacomplex.py is the module of interest and it contains:

Just a side note: PDB/HBPLUS style residue Ids: (chain_name)(residue_position) eg. B24 -> Chain B, position 24
                  DSSR style Ids: (chain_name).(residue_type)(residue_position) eg. B.G24 -> Chain B, resType 'G' or Guanine, position 24.
                                    If te residue_type is something like U34, then a '/' will split it between the resType and position

rnacomplex.DSSRtoPDBid(dssrId) 
    dssrId is a string like 'E.G27', translating it to a tuple containing the PDB residue Id and residue type like ('E27', 'G')
    
rnacomplex.canonTag(canonSymbol):
    canonSymbol is either "|' or '.'. Returns 'cannonical' for '|' and 'noncanonical' for '.'.
    Used for bp_type in helices and stems in the DSSR json

rnacomplex.helixTypeTag(helixSymbol, prefix)
    helixSymbol is a string either "A", "B", "Z", ".", "x" where A, B, and Z are their respective helix types, '.' is for unknown, and 'x' is for broken backbone.
    Returns a tag like canonTag. used for helix_form string in the json for helicies and stems

The bond Class:
rnacomplex.bond(rawString, bondType)
    rawString is the raw string from the hb2 or nb2
    bondType is either 'HB' or 'VdW'
    
rnacomplex.bond.bondType
rnacomplex.bond.

rnacomplex.bond.donorChain
rnacomplex.bond.donorPos
rnacomplex.bond.donorId
rnacomplex.bond.donorRes
rnacomplex.bond.donorAtom
rnacomplex.bond.donorAtomSpecifier
rnacomplex.bond.donorAtomFull

rnacomplex.bond.acceptorChain
rnacomplex.bond.acceptorPos
rnacomplex.bond.acceptorId
rnacomplex.bond.acceptorRes
rnacomplex.bond.acceptorAtom
rnacomplex.bond.acceptorAtomSpecifier
rnacomplex.bond.acceptorAtomFull
