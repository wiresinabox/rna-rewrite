import rnacomplex
import os
import json

rundir = './datafiles/12/'
pdbdir = './pdb/12/'
extrafiles = ['info.txt', 'dssr-extra', 'hbdebug.dat']

dirlist = [fn for fn in os.listdir(rundir) if not fn.startswith('.') and fn not in extrafiles]

#for compname in dirlist:
#    print(compname)
#    hbfile = open(rundir+compname+'/'+compname+'.hb2', 'r')
#    nbfile = open(rundir+compname+'/'+compname+'.nb2', 'r')
#    pdbfile = open(pdbdir+compname+'.pdb', 'r')
#    jsonfile = open(rundir+compname+'/'+compname+'.json', 'r')
    

#    rnaComplex = rnacomplex.rnacomplex(compname, pdbfile, jsonfile, hbfile, nbfile)
#    print(rnaComplex.chainIdCountDict) 
#    print(rnaComplex.resTypeCountDict) 
    
    #for res in rnaComplex.residueDict:
    #    print(rnaComplex.residueDict[res])

compname = '1vq8'

hbfile = open(rundir+compname+'/'+compname+'.hb2', 'r')
nbfile = open(rundir+compname+'/'+compname+'.nb2', 'r')
pdbfile = open(pdbdir+compname+'.pdb', 'r')
jsonfile = open(rundir+compname+'/'+compname+'.json', 'r')

complex1feu = rnacomplex.rnacomplex('1feu', pdbfile, jsonfile, hbfile, nbfile)

#print('--------')
#print(complex1feu.chainIdCountDict)
#print('--------')
#print(complex1feu.residueDict['E80'])
print('--------')

print(complex1feu.residueDict['01678'])


#jsonstr = jsonfile.readline()
#jsondict = json.loads(jsonstr)
#for item in jsondict:
#    print(item)
#print('========')
#jsonitem = jsondict['iloops']
#for item in jsonitem:
#    print('========')
#    for thing in item:
#        print(thing, item[thing])
#    print('--------')
